import pytest
import pytest_html
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import os
from time import sleep
from selenium.webdriver.support.ui import Select


def test_open_google_url():
    chrome_options = Options()
    chrome_options.add_argument("--headless")
    chrome_driver = os.getcwd() + "\\chromedriver_windows\\chromedriver.exe"

    driver = webdriver.Chrome(options=chrome_options, executable_path=chrome_driver)
    driver.get("https://www.google.com")
    print(driver.title)
    sleep(1)


def test_open_lambdatest_url():
    chrome_options = Options()
    chrome_options.add_argument("--headless")
    chrome_driver = os.getcwd() + "\\chromedriver_windows\\chromedriver.exe"
    driver = webdriver.Chrome(options=chrome_options, executable_path=chrome_driver)
    driver.get("https://www.lambdatest.com/")
    print(driver.title)
    sleep(1)


def test_exeauto_url():
    chrome_options = Options()
    chrome_options.add_argument("--headless")
    chrome_driver = os.getcwd() + "\\chromedriver_windows\\chromedriver.exe"
    driver = webdriver.Chrome(options=chrome_options, executable_path=chrome_driver)
    driver.get("https://demosite.executeautomation.com/index.html")
    sleep(1)
    print(driver.title)
    driver.find_element_by_id("TitleId").click()
    sleep(1)
    Select(driver.find_element_by_id("TitleId")).select_by_visible_text("Mr.")
    sleep(1)
    driver.find_element_by_id("TitleId").click()
    sleep(1)
    driver.find_element_by_id("Initial").click()
    sleep(1)
    driver.find_element_by_id("Initial").clear()
    sleep(1)
    driver.find_element_by_id("Initial").send_keys("Panda")
    sleep(1)
    driver.find_element_by_id("FirstName").clear()
    sleep(1)
    driver.find_element_by_id("FirstName").send_keys("Bibhudatta")
    sleep(1)
    driver.find_element_by_name("Hindi").click()
    sleep(1)
    driver.find_element_by_name("Save").click()
