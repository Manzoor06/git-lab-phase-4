# Import the 'modules' that are required for execution

import pytest
import pytest_html
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from time import sleep
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException


#Fixture for Firefox
@pytest.fixture(scope="class")
def driver_init(request):
    ff_driver = webdriver.Firefox()
    request.cls.driver = ff_driver
    yield
    ff_driver.close()

#Fixture for Chrome
@pytest.fixture(scope="class")
def chrome_driver_init(request):
    chrome_driver = webdriver.Chrome()
    request.cls.driver = chrome_driver
    yield
    chrome_driver.close()

@pytest.mark.usefixtures("driver_init")
class BasicTest:
    pass
class Test_URL(BasicTest):
        def test_open_url(self):
            self.driver.get("https://www.lambdatest.com/")
            print(self.driver.title)

            sleep(5)

@pytest.mark.usefixtures("chrome_driver_init")
class Basic_Chrome_Test:
    pass
class Test_URL_Chrome(Basic_Chrome_Test):
        def test_open_url(self):
            self.driver.get("https://www.lambdatest.com/")
            print(self.driver.title)

            sleep(5)
@pytest.mark.usefixtures("chrome_driver_init")
class Basic_Chrome_Test:
    pass
class TestCase2(Basic_Chrome_Test):
		def test_case2(self):
			driver = self.driver
			driver.get("https://demosite.executeautomation.com/index.html")
			sleep(2)
			print(self.driver.title)
			driver.find_element_by_id("TitleId").click()
			sleep(2)
			Select(driver.find_element_by_id("TitleId")).select_by_visible_text("Mr.")
			sleep(2)
			driver.find_element_by_id("TitleId").click()
			sleep(2)
			driver.find_element_by_id("Initial").click()
			sleep(2)
			driver.find_element_by_id("Initial").clear()
			sleep(2)
			driver.find_element_by_id("Initial").send_keys("Panda")
			sleep(2)
			driver.find_element_by_id("FirstName").clear()
			sleep(2)
			driver.find_element_by_id("FirstName").send_keys("Bibhudatta")
			sleep(2)
			driver.find_element_by_name("Hindi").click()
			sleep(2)
			driver.find_element_by_name("Save").click()
			#driver.close()